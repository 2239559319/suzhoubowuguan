const enum AppointStatus {
  Allow = 0,
  Full = 3,
  Expire = -1
}

interface AppointTime {
  surplusId: string;
  surplusCount: number;
  intervalDate: string;
  surplusCountTeam: number;
  intervalValue: string;
}

export interface AppointDate {
  dayTime: string;
  dayWeek: number;
  status: AppointStatus;
  surplusList?: AppointTime[];
}

export interface Customer {
  customerId: string;
}
