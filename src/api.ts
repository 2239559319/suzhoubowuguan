import apiService from './apiService';
import { companyInfoId, systemId } from './constant';
import { AppointDate, Customer } from './type';

export async function getLoginImg(): Promise<{ image: string; vid: string }> {
  const url = '/japi/sw-saas-cloud/saas/queryVCode';
  const res = await apiService.get(url);
  const img = res.data.data;
  return img;
}

/**
 * 发送验证码到手机
 */
export async function getLoginPhoneCode(params: {
  phone: string;
  vcode: string;
  vid: string;
}) {
  const url = '/japi/sw-saas-cloud/saas/querySmsVCode';
  await apiService.get(url, params);
}

export async function login(params: {
  phone: string;
  svCode: string;
  openId: string;
  openIdType: string;
}): Promise<{ authorizationc: string; defaultPassword: boolean; pwd: string }> {
  const url = '/japi/sw-saas-cloud/saas/svcodeLogin';

  const res = await apiService.post(url, {}, params);
  return res.data;
}

export async function getDateList(): Promise<AppointDate[]> {
  const url = '/japi/sw-trm-cloud/api/personalReserve/queryPersonal';
  const res = await apiService.post(url, { systemId, companyInfoId });
  return res.data;
}

export async function getCustomerList(): Promise<Customer[]> {
  const url = '/japi/sw-saas-cloud/customerContact/queryList';
  const res = await apiService.post(url, { systemId });
  return res.data;
}
