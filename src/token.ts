import CryptoJS from 'crypto-js';

const AES = CryptoJS.lib.Cipher._createHelper(CryptoJS.algo.AES as any);
const keyStr = 'ZW8H3S4SZ2ZCB02W02UAWH3BHGDIT8VY';
const key = CryptoJS.enc.Utf8.parse(keyStr);

function getRandomStr(): string {
  let s = '';
  for (let i = 0; i < 6; i++) {
    const num = Math.floor((Math.random() * 10));
    s += num.toString();
  }
  return s;
}

export function getToken() {
  const timeStr = new Date().getTime().toString();

  const message = getRandomStr() + timeStr;
  const token = AES.encrypt(message, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  });
  const tokenStr = token.toString();
  return tokenStr;
}
