import axios, { Axios } from 'axios';
import { appid } from './constant';
import { getToken } from './token';

export class ApiService {
  axiosInstance: Axios;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: 'https://newticket.szmuseum.com',
      headers: {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0.0; MI 5s Plus Build/OPR1.170623.032; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/86.0.4240.99 XWEB/3262 MMWEBSDK/20210302 Mobile Safari/537.36 MMWEBID/5718 MicroMessenger/8.0.2.1860(0x28000234) Process/appbrand2 WeChat/arm32 Weixin NetType/WIFI Language/zh_CN ABI/arm64 MiniProgramEnv/android',
        'Content-Type': 'application/json',
        'Host': 'newticket.szmuseum.com'
      }
    });

    this.axiosInstance.interceptors.response.use((response) => {
      if (response.data.code !== 200) {
        const msg = response.data.msg;
        console.error(msg);

      }
      return response;
    })
  }

  get(url, params?) {
    return this.axiosInstance.get(url, {
      params,
      headers: {
        appid,
        token: getToken(),
      }
    });
  }

  async post(url, data, params?) {
    const res = await this.axiosInstance.post(url, JSON.stringify(data), {
      headers: {
        appid,
        token: getToken(),
      },
      params,
    });
    return res.data;
  }
}

const apiService = new ApiService();
export default apiService;
